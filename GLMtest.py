import matplotlib
# matplotlib.use("Agg")
from bolideSolve import *

''' data for 04 May 2019 over Gulf '''
ref    = datetime.datetime(2019,5,4,15,35,40,000000)
GLMref = datetime.datetime(2019,5,4,15,35,40,0)

GLM16pos = get_GLMeph('G16_eph.nc')
GLM16 = GLMobs("GLM 16", GLM16pos, 'GLM16_04May.csv', GLMref, ref)
GLM16.name = "GLM 16"

### since GLM17 eph is not available, just rotate the GLM16 data
### by 62 deg westward
GLM17pos = rotationAbout(GLM16pos, np.array([0., 0., 1.]), -np.radians(62.))
GLM17 = GLMobs("GLM 17", GLM17pos, 'GLM17_04May.csv', GLMref, ref)
GLM17.name = "GLM 17"


''' ADD THE LIST OF OBSERVATIONS HERE '''
Obs = [GLM16,GLM17]

''' RUN THE SOLVER '''
solver = triangulate(Obs, ref)
solver.minimize_solution()


''' 
    GET THE ENERGY ESTIMATE FROM GLM DATA 
    the second parameter is the pixel resolution
    on the ground from the camera    
'''

GLM16.get_energy(solver, 8000.)
GLM17.get_energy(solver, 14000.)

''' PLOT THE RESULTS '''
solver.do_plots()


### PLOT GLM magnitudes 
figM = plt.figure()
axM  = figM.add_subplot(111)
axM.plot(GLM16.t, GLM16.mag, 'k-')
axM.plot(GLM17.t, GLM17.mag, 'r-')
axM.set_ylim(axM.get_ylim()[::-1])
axM.set_xlabel("Time [s]")
axM.set_ylabel(r"Magnitude")
figM.tight_layout()
figM.savefig("plots/GLMmag.png")

### PLOT GLM SED
figSED = plt.figure()
axSED  = figSED.add_subplot(111)
axSED.plot(GLM16.t, GLM16.SED*1.e6, 'k-')
axSED.plot(GLM17.t, GLM17.SED*1.e6, 'r-')
axSED.set_xlabel("Time [s]")
axSED.set_ylabel(r"SED [$\mu$J/m$^2$/ster/nm]")
figSED.tight_layout()
figSED.savefig("plots/GLMSED.png")

### PLOT GLM energy
figenergy = plt.figure()
axenergy  = figenergy.add_subplot(111)
axenergy.plot(GLM16.t, GLM16.energy*1.e15, 'k-')
axenergy.plot(GLM17.t, GLM17.energy*1.e15, 'r-')
axenergy.set_xlabel("Time [s]")
axenergy.set_ylabel(r"energy [fJ]")
figenergy.tight_layout()
figenergy.savefig("plots/GLMenergy.png")