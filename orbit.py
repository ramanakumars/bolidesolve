import numpy as np
import scipy as sp

## TODO
#	Check crash
#	Plot

def project(u):
    unorm = np.asarray(u)/np.linalg.norm(u)
    
    r = np.array([unorm[0],unorm[1]])
    
    theta = np.arctan2(unorm[2],np.linalg.norm(r))
    phi = np.arctan2(r[1],r[0])

    return np.asarray([theta, phi])

## x-z plane is the meridian
## rotate about a vector in the global coordinate system
def rotationAbout(v, u, angle):
    theta, phi = project(u)
    lati = np.pi/2. - theta
    rotation1 = np.array([[np.cos(phi),-np.sin(phi),0.],[np.sin(phi),np.cos(phi),0.],[0.,0.,1.]])
    rotation2 = np.array([[np.cos(lati),0.,np.sin(lati)],[0.,1.,0.],[-np.sin(lati),0.,np.cos(lati)]])
    rotation3 = np.array([[np.cos(angle),-np.sin(angle),0.],[np.sin(angle),np.cos(angle),0.],[0.,0.,1.]])
    rotateaxes = np.dot(rotation1,rotation2)
    
    #print(u, np.round(np.dot(rotateaxes, np.asarray([0., 0., 1.])),5))
    
    v = np.asarray(v).reshape(1,3)
    vT = v.T
    final = np.dot(np.dot(rotateaxes,rotation3),np.dot(rotateaxes.T,vT)).reshape(3)
    
    return np.round(final,10)

class Units:
	def __init__(self):
		return None
		
	def set_units(self, mass, du, tu):
		self.du = du
		self.tu = tu
		self.object_mass = mass
		self.get_mu()
		
	def get_can_units(self, object = "Earth"):
		if(object in ["Earth","earth","EARTH"]):
			self.du = 6378100
			self.object_mass = 5.98*10**24
			mu_m = (6.67*(10**(-11)))*self.object_mass
			t_orb = np.sqrt(4*np.pi**2/mu_m * (self.du)**3)
			v_orb = 2*np.pi*self.du/t_orb
			self.tu = self.du/v_orb
			
		self.get_mu()
		
	def get_mu(self):
		if(self.du < 10**3):
			rad = 10**7
		else:
			rad = self.du
		
		mu_m = (6.67*(10**(-11)))*self.object_mass
		
		t_sqr = (4*np.pi**2)/mu_m * ((rad)**3)
		
		self.mu = (4*np.pi**2)*(rad/self.du)**3/(t_sqr/self.tu**2)
		
		return self.mu
	
	def convert_vel(self, v):
		return (v*(self.tu/self.du))
	
	def convert_sp_ang_mom(self, h):
		return (h* (self.tu/self.du**2))
		
	def convert_energy(self, e):
		return (e * (self.tu/self.du)**2)
	
class Orbit:

	def __init__(self):
		pass
		
	def create_from_rv(self,r,v, units):
		if(len(r) == 2 and len(v) == 2):
			r[2] = 0
			v[2] = 0
		elif(len(v) == 2):
			v[2] = 0
		elif(len(r) == 2):
			r[2] = 0
		elif(len(r) == 3 and len(v) == 3):
			pass
		else:
			return None
		
		self.r = r
		self.v = v
		
		self.units = units
		
		self.mu = self.units.mu
		
		self.r_mag = np.linalg.norm(self.r)
		# print("Radius: ", self.r_mag)
		
		self.v_mag = np.linalg.norm(self.v)
		# print("Speed: ", self.v_mag)
		
		self.get_basic_elem()
		
		self.inclination_nodes()
		
		self.get_peri_apo()
		
		if(self.periapsis_mag < 1):
			self.get_crash()
	
	def get_basic_elem(self):
		self.sp_energy = 0.5*(self.v_mag**2.) - self.mu/(self.r_mag)
		# print("Specific Energy: ", np.around(self.sp_energy,5))
		
		self.sp_ang_mom = np.cross(self.r,self.v)
		
		self.sp_ang_mom_mag = np.linalg.norm(self.sp_ang_mom)
		# print("Specific angular momentum: ", self.sp_ang_mom, "|", np.around(self.sp_ang_mom_mag,5))
		
		self.ecc_vec = (1./self.mu)*(((self.v_mag**2. - (self.mu/self.r_mag))*self.r) - (np.dot(self.r,self.v)*self.v))
		
		# print("Eccentricity vector: ", self.ecc_vec)
		
		self.lat_rec = (self.sp_ang_mom_mag**2.)/self.mu
		
		# print("Latus rectum: ", np.around(self.lat_rec,4))
		
		# if(self.sp_ang_mom_mag <= 0):
			# print("Negative or zero angular momentum! No orbit")
		
		self.ecc = np.sqrt(1. + 2.*(self.sp_energy)*(self.sp_ang_mom_mag**2.)/(self.mu**2.))
		# print("Eccentricity: ", np.around(self.ecc,4))
		
		self.semi_major = self.lat_rec/(1. - self.ecc**2.)
		# print("Semi-major axis: ", np.around(self.semi_major,4))
	
	def inclination_nodes(self):
		theta, phi = project(self.sp_ang_mom/self.sp_ang_mom_mag)
		self.incangle = np.pi/2. - theta
		self.incline = (np.pi/2. - np.abs(theta))
		# print("Inclination angle: ", np.around(self.incline*(180/np.pi),3))
			
		self.node = np.cross(np.asarray([0.,0.,1.]),self.sp_ang_mom/self.sp_ang_mom_mag)
		if(np.linalg.norm(self.node) != 0):
			self.node = self.node/np.linalg.norm(self.node)
			# print("Ascending node: ", self.node)
			self.node_dir = np.arctan2(self.node[1],self.node[0])
			if(self.node_dir < 0.):
				self.node_dir += 2.*np.pi
			# print("Ascending node angle: ", np.around(self.node_dir * (180/np.pi), 2))
		if(self.ecc != 0 and np.linalg.norm(self.node) != 0):
			perirotated = rotationAbout(self.ecc_vec/self.ecc, self.node, -self.incline)
			theta, phi = project(perirotated)
			self.peri_angle = phi - self.node_dir
			if(self.peri_angle < 0.):
				self.peri_angle += 2.*np.pi
			# print("Argument of Periapsis: ", np.around(self.peri_angle*(180/np.pi),3))

	def get_nu_from_r(self, r):
		vec = r.copy()
		if(self.incline != 0.):
			vec = rotationAbout(r, self.node, -self.incline)
		theta, phi = project(vec)

		return phi

	def get_v_from_nu(self, nu, d=0):
		if(d == 1):
			nu = np.radians(nu)
		r = self.get_r_from_nu(nu)
		sp = self.get_speed_at_r(r)
		phi = np.arcsin(self.sp_ang_mom_mag/(r*sp))

		pos = self.get_pos_nu(nu)
		v = rotationAbout((pos/r)*sp, self.sp_ang_mom, phi)

		perinorm = self.get_pos_nu(self.nu_0)/self.get_r_from_nu(self.nu_0)

		eccnorm = (1./self.ecc)*(1./self.mu)*(((sp**2. - (self.mu/r))*pos) - (np.dot(pos,v)*v))

		diff = np.linalg.norm(perinorm - eccnorm)/self.sp_ang_mom_mag
		if(diff < 1.e-6):
			return v
		else:
			return rotationAbout((pos/r)*sp, self.sp_ang_mom, np.pi - phi)

	def get_peri_apo(self):
		self.periapsis_mag = self.lat_rec/(1. + self.ecc)
		
		self.periapsis = (self.periapsis_mag/self.ecc)*self.ecc_vec

		self.nu_0 = self.get_nu_from_r(self.periapsis/self.periapsis_mag)
		
		# print("Longitude of periapsis: ", np.around(self.nu_0 * (180/np.pi),2))

		self.nu_r = self.get_nu_from_r(self.r/self.r_mag)

		# print ("Nu: %.4f"%(self.nu_r*180./np.pi))
		
		# print(self.get_pos_nu(self.node_dir)/self.get_r_from_nu(self.node_dir))
		# print(self.get_nu_from_r(self.node*self.get_r_from_nu(self.node_dir))*180./np.pi)

		# print("\n")
		
		# print("Periapsis: ", np.around(self.periapsis,3), "|", self.periapsis_mag)
		
		if(self.ecc < 1):
		
			apoapsis_arg = self.nu_0 + np.pi
			
			self.apoapsis_mag = self.lat_rec/(1 - self.ecc)

			self.apoapsis = self.get_pos_nu(apoapsis_arg)
			
			# print("Apoapsis: ", np.around(self.apoapsis,3), "|", self.apoapsis_mag)
		
	def get_pos_nu(self, nu, d = 0):
		if(d == 1):
			nu = np.radians(nu)
		vec = np.asarray([np.cos(nu),np.sin(nu), 0.])
		if(self.incline != 0. ):
			vec = rotationAbout(vec, self.node, self.incline)
		mag = np.abs(self.lat_rec/(1. + self.ecc*np.cos(nu-self.nu_0)))
		return mag*vec
		
	def one_stat_measure(self, p, a, e, dp, da, de, sidereal, t, height, lat, long, units):
		theta = sidereal + int + (360*t)/(23.93)
		theta = theta *(np.pi/180)
		lat = lat *(np.pi/180)
		
		a = a*(np.pi/180)
		e = e*(np.pi/180)
		
		da = da*(np.pi/180)
		de = de*(np.pi/180)
		
		rot_mat = [[np.sin(lat)*np.cos(theta), -np.sin(theta), np.cos(lat)*np.cos(theta)],[np.sin(lat)*np.sin(theta), np.cos(theta), np.cos(lat)*np.sin(theta)],[-np.cos(lat), 0, np.sin(lat)]]
		
		p_sez = p * np.around(np.asarray([-np.cos(e)*np.cos(a),np.cos(e)*np.sin(a), np.sin(e)]),4)
		p_ijk = np.dot(rot_mat, p_sez)
		
		print("Rotation matrix: ", np.around(rot_mat, 3))
		
		r_earth = (6378100)/units.du
			
		x = ((r_earth)/np.sqrt(1 - 0.006694*np.sin(lat)) + height)*np.cos(lat)
		z = ((r_earth)*(0.993057)/np.sqrt(1 - 0.006694*np.sin(lat)) + height)*np.sin(lat)
		
		R_ijk = np.around([x*np.cos(theta), x*np.sin(theta), z],4)
		
		r = p_ijk + R_ijk
		
		dP_s = dp*np.cos(e)*np.cos(a) + p*de*np.sin(e)*np.cos(a) - p*da*np.cos(e)*np.sin(a)
		dP_e = dp*np.cos(e)*np.sin(a) + p*de*np.sin(e)*np.sin(a) - p*da*np.cos(e)*np.cos(a)
		dP_z = dp*np.sin(e) + p*de*np.cos(e)
		
		dP_sez = [dP_s, dP_e,dP_z]
		
		dP_ijk = np.dot(rot_mat, dP_sez)
		
		v = np.cross([0,0,(np.pi)/(86164)],r) + dP_ijk
		
		print("r_SEZ: ", np.around(np.dot(np.linalg.inv(rot_mat),r)/r_earth,3))
		print("v_SEZ: ", np.around(np.dot(np.linalg.inv(rot_mat),v)* (806.8)/(r_earth),3))
		
		print("In Earth canonical units: ")        
		print("R_IJK: ", np.around(R_ijk/r_earth, 3))
		print("r_SEZ: ", np.around(np.asarray(p_sez)/r_earth, 3))
		print("v_SEZ: ", np.around(np.asarray(dP_sez) * (806.8)/(r_earth), 3))
		print("r_IJK: ", np.around(r,3))
		print("v_IJK: ", np.around(v,3))
		
		print("\n")
		self.create_from_rv(r,v)
		
	def gibbs_method(self, r1, r2, r3, units):
		r1 = np.asarray(r1)
		r2 = np.asarray(r2)
		r3 = np.asarray(r3)
	
		r1_mag = np.linalg.norm(r1)
		r2_mag = np.linalg.norm(r2)
		r3_mag = np.linalg.norm(r3)
		
		N = r3_mag*np.cross(r1,r2) + r2_mag*np.cross(r3, r1) + r1_mag*np.cross(r2, r3)
		D = np.cross(r1,r2) + np.cross(r3, r1) + np.cross(r2, r3)
		
		S = (r2_mag - r3_mag)*r1 + (r3_mag - r1_mag)*r2 + (r1_mag - r2_mag)*r3
		
		print(N)
		print(D)
		print(S)
		
		N_mag = np.linalg.norm(N)
		D_mag = np.linalg.norm(D)
		S_mag = np.linalg.norm(S)
		
		lat_rec = N_mag/D_mag
		
		mu = units.mu
		
		v1 = np.sqrt(mu/lat_rec) * (np.cross((D/D_mag), r1/r1_mag) + S/D_mag)
		v2 = np.sqrt(mu/lat_rec) * (np.cross((D/D_mag), r2/r2_mag) + S/D_mag)
		v3 = np.sqrt(mu/lat_rec) * (np.cross((D/D_mag), r3/r3_mag) + S/D_mag)
		
		print("Velocities at each position: ")
		print(v1)
		print(v2)
		print(v3)
		
		self.create_from_rv(r1,v1, units)
		
	def set_orbit_elem(self, p,e, i, node_angle, nu_0, nu_r, units, d = 1):
		if(d == 1):
			i = np.radians(i)
			node_angle = np.radians(node_angle)
			nu_0 = np.radians(nu_0)
			nu_r = np.radians(nu_r)
			
		self.lat_rec = p
		self.ecc = e
		self.incline = i
		self.node_dir = node_angle
		self.nu_0 = nu_0
		self.peri_angle = node_angle - nu_0
		self.nu_r = nu_r
		self.units = units
		self.mu = units.mu
		self.sp_energy = -self.mu*(1.-e**2.)/(2.*p)

		self.node = np.asarray([np.cos(node_angle), np.sin(node_angle),0.])
		self.sp_ang_mom_mag = np.sqrt(p*self.mu)
		self.sp_ang_mom = self.sp_ang_mom_mag*rotationAbout(np.asarray([0., 0., 1.]), self.node, i)
		
	def perifocal_r_v(self):
		r_mag = self.lat_rec/(1 + self.ecc * np.cos(self.nu_r))
		self.r_peri = r_mag *np.asarray([np.cos(self.nu_r),np.sin(self.nu_r),0])
		self.v_peri = np.sqrt(1/self.lat_rec)*np.asarray([-np.sin(self.nu_r),(self.ecc + np.cos(self.nu_r)),0])
		
	def transform_peri_geo(self, run = 1):
		rot_1 = [[np.cos(self.peri_angle),-np.sin(self.peri_angle),0],[np.sin(self.peri_angle),np.cos(self.peri_angle),0],[0,0,1]]
		r_1_ijk = np.dot(rot_1, self.r_peri)
		rot_2 = [[1,0,0],[0,np.cos(self.incline),-np.sin(self.incline)],[0,np.sin(self.incline),np.cos(self.incline)]]
		r_2_ijk = np.dot(rot_2, r_1_ijk)
		rot_3 = [[np.cos(self.node_dir),-np.sin(self.node_dir),0],[np.sin(self.node_dir),np.cos(self.node_dir),0],[0,0,1]]
		r_ijk = np.dot(rot_3, r_2_ijk)
		
		v_1_ijk = np.dot(rot_1, self.v_peri)
		v_2_ijk = np.dot(rot_2, v_1_ijk)
		v_ijk = np.dot(rot_3, v_2_ijk)
		
		if(run == 1):
			self.create_from_rv(r_ijk, v_ijk)
		else:
			print(np.around(r_ijk,3))
			print(np.around(v_ijk,3))
		
	def get_crash(self):
		return 0
		
	def get_speed_at_r(self,r):
		return np.sqrt(2*(self.sp_energy + self.mu/r))
	
	def get_flight_path_angle(self, r):
		v = self.get_speed_at_r(r)
		return (self.sp_ang_mom_mag/(v*r))
	
	def get_r_from_nu(self, nu):
		return np.linalg.norm(self.get_pos_nu(nu))

	def time_flight(self, nu1 = 0 , nu2 = 0, r1 = 0, r2 = 0):
		if(self.ecc < 1):
			if(nu1 == 0 or nu2 == 0):
				print("ERROR, please give true anomaly for elliptical orbits!")
				return 0;
			cosE1 = (self.ecc + np.cos(nu1))/(1+self.ecc*np.cos(nu1))
			r1 = self.get_r_from_nu(nu1)
			E1 = np.arccos(cosE1)
			
			cosE2 = (self.ecc + np.cos(nu2))/(1+self.ecc*np.cos(nu2))
			E2 = np.arccos(cosE2)
			
			x = np.sqrt(self.semi_major)*(E2 - E1)
		elif(self.ecc == 1):
			if(nu1 == 0 or nu2 == 0):
				print("ERROR, please give true anomaly for parabolic orbits!")
				return 0;
			r1 = self.get_r_from_nu(nu1)
			D2 = np.sqrt(self.lat_rec)*np.tan(nu2/2)
			D1 = np.sqrt(self.lat_rec)*np.tan(nu1/2)
			
			x = D2 - D1
		elif(self.ecc > 1):
			if(r1 == 0 or r2 == 0):				
				r2 = self.get_r_from_nu(nu2)
				r1 = self.get_r_from_nu(nu1)
			F2 = np.arccosh((1 - r2/self.semi_major)/self.ecc)
			F1 = np.arccosh((1 - r1/self.semi_major)/self.ecc)
			
			x = np.sqrt(-self.semi_major)*(F2 - F1)
			
		z = x**2/self.semi_major
		
		if(z > 0):			
			S = (np.sqrt(z) - np.sin(np.sqrt(z)))/np.sqrt(z**3)
			C = (1 - np.cos(np.sqrt(z)))/z
		elif(z < 0):
			C = (1 - np.cosh(np.sqrt(-z)))/z
			S = (np.sinh(np.sqrt(-z)) - np.sqrt (-z))/np.sqrt(-z**3)
		else:
			S = 1/6
			C = 1/2
			
		rdotv = (r1 * self.get_speed_at_r(r1) * np.sqrt(1 - (self.get_flight_path_angle(r1))**2))
		
		t = (1/np.sqrt(self.mu))*((x**3)*S  + rdotv/self.mu * x**2 * C + r1*x*(1-z*S))
		
		print("Time of flight: ", np.around(t, 3))
		
		return t
		
		
		
# r = [2,0,0.2]
# v = [0,0.5,0]
# orb = Orbit()
# unit = Units()
# #unit.set_units(5.972*10**24, 6378100,806.815)
# unit.get_can_units(object = "earth")
# orb.create_from_rv([1.,0,0],[0,1.5,0], units = unit)


'''
## INSTRUCTIONS ##
orb = Orbit() starts the class. From here, you can use the object to run orbit determination.

If you have one radius and velocity vector, use the function orb.create_from_rv(r,v)
r and v can be 2-d or 3-d vector defined as [x,y,z]
If you are not using canonical units, use orb.create_from_rv(r,v, can = 0), which runs the programs in METERS. Make sure you're r, v are in meters

For 1-station measurement use orb.one_stat_measure(p, a, e, dp, da, de, sidereal, t, height, lat, long). Here, t is the time (in HOURS) to 0000 GMT January 1st. Long is longitude east and lat is latiude north. 
If you are not using canonical units, add can = 0 like above, which converts the r and v to canonical units before running orbit determination.

For Gibbs method use orb.gibbs_method(r1,r2,r3). Add can = 0 if you are using meters.
'''