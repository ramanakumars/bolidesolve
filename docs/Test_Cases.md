## Good Test Case
This is an example of a bolide event with a good solution-- the 31 March 2019 event over North Florida.  

In order to analyze this event, we start with setting up the `GLMtest.py` code.

First thing we need to update is the `ref` time.  This time provides the code a reference 'start time' for the event.  This time can be approximated from the start times in the data provided-- recommend starting 500 ms before actual event start time to make sure to capture entire track.   

If incorporating GLM data in with the trajectory and orbit solution, you must update the 'GLMref' time using the same time as assigned to the file when retrieving it from the NOAA CLASS system.   The GLM data batches are issued in 20-second intervals, so milliseconds will remain set to 0.  

```python
ref    = datetime.datetime(2019,3,31,3,54,5,000000)
GLMref = datetime.datetime(2019,3,31,3,54,0,0)

```
The next step is to create our observations (objects?).
There are currently three sources of observational data: SkySentinel Data, CAMS data, and GLM data.  As such, there are three different choices of 'observation' to create for the trajectory calculation.  For this example event, we will use three different SkySentinel data files, and one set of CAMS data.  

If you have SkySentinel data, your task is to create a `SkySentinelObs` object, named for the node from which the data came.  If you have CAMS data, you can also make a `CAMSObs`.  Both types of observations require you to point to the datafile, give a ref time (seen here as just `ref` as set above), and a time offset if required for best alignment (in seconds).  

```python
node10 = SkySentinelObs('node10', 'node10dat.csv', ref, -0.10)
node34 = SkySentinelObs('node34', 'node34dat.csv', ref, -0.10)
node42 = SkySentinelObs('node42', 'node42dat.csv', ref, 0.10)
CAMS5003 = CAMSObs('CAMS5003', 'CAMS5003dat.csv', ref, 0.0)
```

Note the data file name, 'node10dat.csv' string, needs to be updated to your data file name.  System will default to run directory; make sure to note location if different.  

Similarly, If we're using GLM data, we need to integrate it into the code, creating a `GLMObs` object and telling the computer where to find the GLM data files.  Note here, since we're using satellite data, we must also designate the position of the satellite.  This adds an additional objet `GLMPos` which uses ephemeris data from NOAA to describe the position of the satellite.  This data is saved in the 'G16_eph.nc' file.  Also, don't forget to change the name in the  'GLM16_31Mar.csv' to the appropriate file name. Example:
```python
GLM16pos = get_GLMeph('G16_eph.nc')
GLM16 = GLMobs("GLM 16", GLM16pos, 'GLM16_31Mar.csv', GLMref, ref)
GLM16.name = "GLM 16"

GLM17pos = rotationAbout(GLM16pos, np.array([0., 0., 1.]), -np.radians(62.))
GLM17 = GLMobs("GLM 17", GLM17pos, 'GLM17_31Mar.csv', GLMref, ref)
GLM17.name = "GLM 17"
```
**Because ephemeris data was not available for GOES-17 at the time this code was written, the GLM16 data was rotated suitably and set here to match GLM17's position.

Next we need to tell the system which observations to use.  Say we like how node42, node10, and CAMS5003 are shaping up, but node34 has too much noise.  We can designate which nodes we want included in the calculation by including (or excluding) nodes in the `observation list`:

```python

''' ADD THE LIST OF OBSERVATIONS HERE '''
Obs = [node10, node42, CAMS5003]

```
note that the same name that was used in designating the observation object needs to be used here in the list of observations.  

Last step: under '''RUN THE SOLVER'''
make sure you have the following listed after the observation list:

```python
solver = triangulate(Obs, ref)
solver.minimize_solution()
```

If you'd like to plot trajectory and orbit, make sure this appears under '''PLOT THE RESULTS'''
```python
solver.do_plots()
solver.get_orbit(planets=["Venus", "Earth", "Mars", "Jupiter"])
```



## Bad Test Case: 04 April 2019 Bolide Event
```python
```