The code accepts three formats as input:

### SkySentinel
The SkySentinel format is contains the following columns: date/time (yyyymmdd_hhmmss) in UTC, # of pixels above threshold, sum of pixels above threshold, x coordinate of brightest pixel, y coordinate, azimuth, altitude. Sometimes also includes RA and DEC. 

### CAMS
The CAMS format is similar to SkySentinel but always contains RA/DEC info.

### GLM
The GLM format contains the following columns: time offset from start of file, latitude, longitude and energy (in J). The start of file is prescribed as input to the GLM observations and is given in the input netCDF data.