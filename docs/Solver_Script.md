The main driver for the solver is similar to the script `examples/GLMtest.py`. To run the code, you need the datafiles (.csv format) for either a GLM, SkySentinel or CAMS observation, the bolidesolve.py file, orbit.py file. If you use GLM, you need the corresponding ephemeris data.

## Header information
We need the functions from the bolidesolve.py code, so add the following for header:
```python
from bolidesolve import *
```

## Time reference
The reference time is one that is before any of the timestamps in the data. It is best to choose one that is about one second before the first timestamp. For e.g. if node 10 is the one that saw the bolide first at 2019-08-30 10:56:30.7 UTC, then a good reference time would be 10:56:30. This is defined using the `datetime` object in Python
```python
ref = datetime(year, month, day, hour, minute, seconds, microseconds)
```
For example, the above example:
```python
ref = datetime(2019, 08, 30, 10, 56, 0, 0)
```

## Inputting the observations

### SkySentinel and CAMS
The observations are defined using either the `SkySentinelObs`, `CAMSObs` or `GLMObs` classes for the different input formats. The `SkySentinelObs` and `CAMSObs` have similar required inputs:

- Name of the node (e.g. 'node10' or 'CAMS5004')

- Input data file (e.g. 'data/node10dat.csv')

- Reference time (defined as `ref` above)

Optionally, you can also force time offset for both these classes (if the node's timing is off) using the parameter `toffset`. 

The SkySentinel class also contains an optional `RADECflag`. Setting this flag to `False` forces the use of the `astropy` module to get RA/DEC from Alt/Az. Setting it to `True` or leaving it as default (`None`) lets the code use the given RA/DEC or computes it from `astropy` if not given. 

For example:
```python
node10 = SkySentinelObs('node10', 'data/node10dat.csv', ref)
```
Create a different object for each input node.

### GLM
For the GLM data, the position needs to first be defined. This is done using the ephemeris data ('G16_eph.nc' is provided but you can download more from [CLASS](https://www.avl.class.noaa.gov/saa/products/search?sub_id=0&datatype_family=GRICSAT&submit.x=28&submit.y=11)). So first define the position as:

```python
GLM16pos = get_GLMeph('G16_eph.nc')
```
replace the 'G16_eph.nc' with any other ephemeris file. 

Currently GOES-17 does not ephemeris data, so we just the GOES-16 data by 62 degrees:
```python
GLM17pos = rotationAbout(GLM16pos, np.array([0., 0., 1.]), -np.radians(62.))
```

Next, define the start time for the GLM file. This is required since the time in the GLM data is defined from this reference time. It is created in the same as way as the date reference:
```python
GLMref = datetime.datetime(2019, 8, 30, 10, 50, 20, 0)
```
 

With the positions and reference defined, we can input the data. This is done using the `GLMObs` class. The required inputs are: 

- Name of the satellite ('GLM16' or 'GLM17')

- Position vector (`GLM16pos` or `GLM17pos` variable defined above)

- Input data file (e.g. 'data/GLM16_dat.csv')

- GLM file start time (`GLMref` variable defined above)

- Reference time (defined as `ref` above)

For example:

```python
GLMref = datetime.datetime(2019,5,4,15,35,40,0)
GLM16pos = get_GLMeph('G16_eph.nc')
GLM16 = GLMobs("GLM 16", GLM16pos, 'GLM16_04May.csv', GLMref, ref)
```

## Creating a list of observations
Create a list with all the input data for the solver to loop through. For example, with SkySentinel nodes 10, 34 and 42:
```python
Obs = [node10, node34, node42]
```

## Solve and do the plots
Now with all the observations created and added, we initialize the solver and get the solution. The `triangulate` class runs the main code, and requires the `Obs` list and `ref` reference time object to run.  Once it is initialized, use the method `minimize_solution` to solve for the trajectory.
```python
solver = triangulate(Obs, ref)
solver.minimize_solution()
```

Then, use the `do_plots` method to create a list of plots for the result. The plots generated are in the plots/ folder (which is created by default) and contains the velocity and height vs. time, lat/lon for each observation and a fit, length residual from the average trajectory, residual in each coordinate direction from the average trajectory and a map showing trajectory and observing nodes. 
```python
solver.do_plots()
```