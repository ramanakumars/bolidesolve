## Getting started

You can download the code by clicking the Settings menu in the upper right corner of the source page, and clicking "Download Repository". This generates a zip file of the source code.

Alternatively, you can clone the repository by opening a terminal and typing (without the $):

```bash
$ git clone https://bitbucket.org/ramanakumars/bolidesolve.git
```

This creates a folder called 'bolideSolve' with the source code. 

## Dependencies
The following Python 3.x modules are required:

- numpy

- scipy

- matplotlib

- astropy

- callhorizons

- rebound

- cartopy

- pyproj

All of them can be installed using pip, e.g. on Linux:

```bash
$ python3 -m pip install numpy scipy matplotlib astropy callhorizons rebound pyproj cartopy
```

All of the packages except rebound can be installed on Windows. The code can be run on bash on Windows using the [Windows Subsystem for Linux (WSL) module](https://docs.microsoft.com/en-us/windows/wsl/install-win10), which can be installed using the Store app.