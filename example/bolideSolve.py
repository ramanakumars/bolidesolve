import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import datetime, sys, os, re
import netCDF4 as nc
# from orbit import *
import astropy.coordinates as coord
from astropy import units as u
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy import integrate
from astropy.time import Time
from functools import reduce
import pyproj, rebound, itertools
import cartopy.crs as ccrs

plt.rc('text', usetex=True)
plt.rc('font', size=20)
plt.rc('figure',figsize=(8,8))
plt.rc('figure',dpi=200)

## x-z plane is the meridian
## rotate about a vector in the global coordinate system
## v is the rotating vector
def rotationAbout(v, u, angle):
    theta, phi = project(u)
    lati = np.pi/2. - theta
    rotation1 = np.array([[np.cos(phi),-np.sin(phi),0.],[np.sin(phi),np.cos(phi),0.],[0.,0.,1.]])
    rotation2 = np.array([[np.cos(lati),0.,np.sin(lati)],[0.,1.,0.],[-np.sin(lati),0.,np.cos(lati)]])
    rotation3 = np.array([[np.cos(angle),-np.sin(angle),0.],[np.sin(angle),np.cos(angle),0.],[0.,0.,1.]])
    rotateaxes = np.dot(rotation1,rotation2)
    
    #print(u, np.round(np.dot(rotateaxes, np.asarray([0., 0., 1.])),5))
    
    v = np.asarray(v).reshape(1,3)
    vT = v.T
    final = np.dot(np.dot(rotateaxes,rotation3),np.dot(rotateaxes.T,vT)).reshape(3)
    
    return final


def project_h(u):
    ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
    lla = pyproj.Proj(proj='longlat', ellps='WGS84', datum='WGS84')

    x, y, z = u

    lon, lat, alt = pyproj.transform(ecef, lla, x, y, z, radians=True)

    return np.asarray([lat, lon, alt])

def project(u):
    unorm = np.asarray(u)/np.linalg.norm(u)
    
    r = np.array([unorm[0],unorm[1]])
    
    theta = np.arctan2(unorm[2],np.linalg.norm(r))
    phi = np.arctan2(r[1],r[0])

    return np.asarray([theta, phi])


class GLMobs:
    def __init__(self, name, satpos, obsfile, dateobs, dateref):
        self.satpos    = satpos
        self.name      = name

        data = np.loadtxt(obsfile, delimiter=',')

        tdat = data[:,0]
        lond = data[:,2]
        latd = data[:,1]
        self.energy = data[:,3]

        self.t = np.zeros_like(tdat, dtype=np.float64)

        ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
        lla  = pyproj.Proj(proj='longlat', ellps='WGS84', datum='WGS84')     

        self.rhat = np.zeros((self.t.shape[0], 3))
        for i, ti in enumerate(tdat):
            dti    = dateobs + datetime.timedelta(seconds=ti) - dateref
            self.t[i] = float(dti.seconds + dti.microseconds/1.e6)

            x, y, z        = pyproj.transform(lla, ecef, lond[i], latd[i], 18000., radians=False)
            self.rhat[i,:] = self.satpos - np.array([x, y, z])

        self.Rhat = self.satpos

        plt.figure()
        plt.subplot(211)
        plt.plot(self.t, latd, 'k-')
        plt.ylabel("Latitude [deg]")
        plt.subplot(212)
        plt.plot(self.t, lond, 'k-')
        plt.ylabel("Longitude [deg]")
        plt.xlabel("Time [s]")
        plt.tight_layout()
        plt.savefig(self.name+'.plot.png')
        plt.close()


    def get_energy(self, solution, pixel_size):
        dt = self.t

        self.SED = np.zeros_like(dt) 
        self.mag = np.zeros_like(dt) 

        for i, ti in enumerate(dt):
            bolidefiti  = solution.bolidefit(ti)
            dist        = np.linalg.norm(bolidefiti - self.satpos) ## distance from satellite to bolide
            aperture    = 5.5e-2 ## 5.5cm radius aperture
            angsize     = np.pi*(aperture/dist)**2.
            footprint   = (pixel_size*pixel_size) ## get the area on the surface 

            self.SED[i]      = self.energy[i]/(angsize*footprint)

            irradiance       = self.SED[i]

            self.mag[i]      = -14. - 2.5*np.log10(irradiance/15.e-6) ## take 15 W/m2/ster/nm => M_v=-14 (Brown et al 2019)
nodes = { \
    "node6":    {"lat": 35.200408, "lon": -111.655566, "h": 2106.}, \
    "node79":   {"lat": 34.233257, "lon": -111.300560, "h": 1514.}, \
    "node10":   {"lat": 29.669253, "lon": -82.600878, "h": 0.0}, \
    "node34":   {"lat": 28.915778, "lon": -81.125889, "h": 0.0}, \
    "node42":   {"lat": 28.062197, "lon": -80.642078, "h": 0.0}, \
    "node5" :   {"lat": 35.151903, "lon": -106.555686, "h": 1600.}, \
    "node4" :   {"lat": 40.627, "lon": -122.31, "h": 181.}, \
    "node17":   {"lat": 37.3600, "lon": -122.07, "h": 60.}, \
    "CAMS5020": {"lat": 29.16576, "lon": -82.17390, "h": 20.}, \
    "CAMS5003": {"lat": 29.66915, "lon": -82.3752, "h":0.0}
}

class SkySentinelObs:
    ''' Initialize an observation site
    Parameters
    ----------
    node : string
        the name of the SkySentinel node which is doing the observation
        (should match the index in the nodes dict above)
    azaltfile : string
        the filename which contains the az/alt data in SkySentinel format
    dateref : datetime.datetime
        datetime obj with the reference datetime
    toffset : float
        (Optional) time offset of this site in seconds
    RADECflag: boolean
        (Optional) False to force processing of RA/DEC using Astropy
        True  to use values from .csv file (if RA/DEC information exists)
    magfile : str
        (Optional) intensity vs. time file (not required for trajectory)
    
    '''
    def __init__(self, node, azaltfile, dateref, toffset=0.0, RADECflag=None, magfile=None):
        try:
            self.lat = nodes[node]["lat"]
            self.lon = nodes[node]["lon"]
            h        = nodes[node]["h"]
        except KeyError:
            print("No node %s found!"%(node))

        # print(node, self.lat, self.lon, h)
        self.name = node
        self.azaltfile = azaltfile
        self.magfile = magfile
        self.dateref = dateref


        ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
        lla  = pyproj.Proj(proj='longlat', ellps='WGS84', datum='WGS84') 

        ### convert from lat/lon/alt to (x,y,z) geocentric
        x, y, z = pyproj.transform(lla, ecef, self.lon, self.lat, h, radians=False)

        rmag    = np.linalg.norm([x,y,z])
        Rhat    = np.zeros(3)
        Rhat[0] = x
        Rhat[1] = y
        Rhat[2] = z

        x, y, z = pyproj.transform(lla, ecef, self.lon, self.lat, 0., radians=False)

        self.h0   = np.linalg.norm([x,y,z])
        self.h    = rmag - self.h0
        self.Rhat = Rhat

        self.get_obs(toffset)

        if(RADECflag!=None)&(self.RADEC==True):
            self.RADEC = RADECflag

        self.create_vectors()


    def get_obs(self, toffset):
        node = open(self.azaltfile,'r')
        dt = []
        azlist = []
        altlist = []
        ralist = []
        declist = []
        intlist = []

        dtoff = datetime.timedelta(seconds=toffset)

        for line in node:
            outline = line.split(',')
            
            date = outline[0]
            az = outline[5]
            alt = outline[6]

            intlist.append(float(outline[2]))
            
            azlist.append(float(az))
            altlist.append(float(alt))

            if(len(outline)==9):
                ra = outline[7]
                dec = outline[8]

                ralist.append(float(ra))
                declist.append(float(dec))
            
            matchstring = r'([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_([0-9]{3})'
            check = re.search(matchstring,date)
            match = np.array(check.groups(),dtype=np.int32)

            year, month, day, hour, minute, sec, millisec = match
            dtime = datetime.datetime(year, month, day, hour, minute, sec, millisec*1000)
            deltat = dtime + dtoff - self.dateref
            dt.append(float(deltat.seconds+(deltat.microseconds/1.e6)) )

        self.dt = np.asarray(dt,dtype=np.float64)
        self.alt = np.asarray(altlist)
        self.az = np.asarray(azlist)
        self.intensity = np.asarray(intlist)

        if(len(ralist)!=0):
            self.ra = np.asarray(ralist)
            self.dec = np.asarray(declist)
            self.RADEC=True
        else:
            self.RADEC=False


        icut = np.where(((self.alt*self.az) > 0.))[0]
        self.t = self.dt[icut]
        self.az2 = self.az.copy()
        self.alt2 = self.alt.copy()
        self.azold = self.az
        self.altold = self.alt
        self.alt = self.alt[icut]
        self.az = self.az[icut]
        self.intensity = self.intensity[icut]

        if(self.RADEC==True):
            self.ra = self.ra[icut]
            self.dec = self.dec[icut]

        dat = np.zeros((self.dt.shape[0],3))
        dat[:,0] = self.dt
        dat[:,1] = self.azold
        dat[:,2] = self.altold

        np.savetxt(self.azaltfile[:-4]+'time.csv',dat,delimiter=',', fmt='%.5f')

        plt.figure()
        plt.subplot(211)
        plt.plot(self.t, self.alt, 'k-')
        plt.subplot(212)
        plt.plot(self.t, self.az, 'k--')
        plt.savefig(self.azaltfile[:-3]+'plot.png')
        plt.close()

        if(self.magfile != None):
            I = np.genfromtxt(self.magfile, dtype=np.float64, delimiter=',')
            self.I = I[np.where(I > I[0])[0]]
            self.dtI = self.dt[np.where(I > I[0])[0]]


    def create_vectors(self):

        self.rhat = np.zeros((self.t.shape[0], 3))

        if(self.RADEC==True): ## if RADEC info exists then just use it
            ra   = np.radians(self.ra)
            dec  = np.radians(self.dec)
        else:                 ## if not determine from astropy
            az   = self.az
            alt  = self.alt

            ra   = np.zeros_like(self.t)
            dec  = np.zeros_like(self.t)

            for i, ti in enumerate(self.t):
                dtoff = datetime.timedelta(seconds=ti)
                tobs = Time(self.dateref + dtoff, scale='utc', location=('0d','0d'))
                loc  = coord.EarthLocation(lat=self.lat*u.deg, lon=self.lon*u.deg, height=self.h*u.m)
                aa   = coord.SkyCoord(alt=alt[i]*u.deg, az=az[i]*u.deg, obstime=tobs, frame='altaz', location=loc)

                radec = aa.icrs
                ra[i]  = np.radians(radec.ra.value)
                dec[i] = np.radians(radec.dec.value)


        for i, ti in enumerate(self.t):
            rhat2 = np.array([\
                np.cos(ra[i])*np.cos(dec[i]),\
                np.sin(ra[i])*np.cos(dec[i]),\
                np.sin(dec[i])\
            ])

            dtoff = datetime.timedelta(seconds=ti)
            tobs = Time(self.dateref + dtoff, scale='utc', location=('0d','0d'))

            cartrep = coord.CartesianRepresentation(rhat2, unit='m')
            bolicrs = coord.GCRS(cartrep, obstime=tobs)

            bolecef = bolicrs.transform_to(coord.ITRS(obstime=tobs))
            # print(satecef)
            boli    = coord.EarthLocation(*bolecef.cartesian.xyz)

            # LST = tobs.sidereal_time('apparent').value*(np.pi/12.)
            # rotatemLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), -LST)
    
            rhati = np.asarray([boli.x.value, boli.y.value, boli.z.value])
            self.rhat[i,:] = rhati/np.linalg.norm(rhati)

class CAMSObs:
    ''' Initialize an observation site
    Parameters
    ----------
    node : string
        the name of the SkySentinel node which is doing the observation
        (should match the index in the nodes dict above)
    azaltfile : string
        the filename which contains the az/alt data in SkySentinel format
    dateref : datetime.datetime
        datetime obj with the reference datetime
    toffset : float
        (Optional) time offset of this site in seconds
    magfile : str
        (Optional) intensity vs. time file (not required for trajectory)
    
    '''
    def __init__(self, node, azaltfile, dateref, toffset=0.0, magfile=None):
        try:
            self.lat = nodes[node]["lat"]
            self.lon = nodes[node]["lon"]
            h        = nodes[node]["h"]
        except KeyError:
            print("No node %s found!"%(node))

        # print(node, self.lat, self.lon, h)
        self.name = node
        self.azaltfile = azaltfile
        self.magfile = magfile
        self.dateref = dateref


        ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
        lla  = pyproj.Proj(proj='longlat', ellps='WGS84', datum='WGS84') 

        ### convert from lat/lon/alt to (x,y,z) geocentric
        x, y, z = pyproj.transform(lla, ecef, self.lon, self.lat, h, radians=False)

        rmag    = np.linalg.norm([x,y,z])
        Rhat    = np.zeros(3)
        Rhat[0] = x
        Rhat[1] = y
        Rhat[2] = z

        x, y, z = pyproj.transform(lla, ecef, self.lon, self.lat, 0., radians=False)

        self.h0   = np.linalg.norm([x,y,z])
        self.h    = rmag - self.h0
        self.Rhat = Rhat

        self.get_obs(toffset)

        self.create_vectors()

    def get_obs(self, toffset):
        node = open(self.azaltfile,'r')
        lines = node.readlines()

        datestring = lines[12]
        pattern    = r"(FF)_([0-9]{6})_([0-9]{4})([0-9]{2})([0-9]{2})_([0-9]{2})([0-9]{2})([0-9]{2})_([0-9]{3})_([.]?)"

        check      = re.search(pattern, datestring)

        match = np.array(check.groups()[2:9],dtype=np.int32)
        year, month, day, hour, minute, sec, millisec = match
        

        date = datetime.datetime(year, month, day, hour, minute, sec, millisec*1000)

        data = np.loadtxt(self.azaltfile, skiprows=15)
        dt   = data[:,0]/29.97
        ra   = data[:,3]; dec = data[:,4]

        self.t = np.zeros_like(dt)
        self.ra = ra; self.dec = dec

        for i, ti in enumerate(dt):
            dti    = date + datetime.timedelta(seconds=ti) - self.dateref
            self.t[i] = float(dti.seconds + dti.microseconds/1.e6)

    def create_vectors(self):

        self.rhat = np.zeros((self.t.shape[0], 3))
        ra   = np.radians(self.ra)
        dec  = np.radians(self.dec)

        for i, ti in enumerate(self.t):
            rhat2 = np.array([\
                np.cos(ra[i])*np.cos(dec[i]),\
                np.sin(ra[i])*np.cos(dec[i]),\
                np.sin(dec[i])\
            ])

            dtoff = datetime.timedelta(seconds=ti)
            tobs = Time(self.dateref + dtoff, scale='utc', location=('0d','0d'))

            cartrep = coord.CartesianRepresentation(rhat2, unit='m')
            bolicrs = coord.GCRS(cartrep, obstime=tobs)

            bolecef = bolicrs.transform_to(coord.ITRS(obstime=tobs))
            # print(satecef)
            boli    = coord.EarthLocation(*bolecef.cartesian.xyz)

            # LST = tobs.sidereal_time('apparent').value*(np.pi/12.)
            # rotatemLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), -LST)
    
            rhati = np.asarray([boli.x.value, boli.y.value, boli.z.value])
            self.rhat[i,:] = rhati/np.linalg.norm(rhati)

class triangulate():
    '''
    This is the solver which minimizes the trajectory based on multiple (atleast 2) observations.
    Parameters
    ----------
    nobs : int
        number of observations
    lat : numpy.ndarray
        array of latitude in standard format (degrees)
    lon : numpy.ndarray
        array of longitude in standard format (degrees)
    alt : numpy.ndarray
        array of altitude valeus (degrees) 
    az : numpy.ndarray
        array of azimuth values (degrees)
    h0 : numpy.ndarray
        average sea level height (this defines the units of the length scale)
    '''
    def __init__(self, Obs, ref):
        ''' Initialize the model '''

        nobs = 2
        self.nobs    = len(Obs)
        self.Obslist = Obs
        self.ref     = ref

        ## R is the distance from each location from the center of the Earth
        ## Rhat defines the position vector of each camera
        ## rhat defines the direction of the bolide from each observing site
        ## rmag is the distance from each observing site to the bolide
        ## r is the position of the bolide according to each observation's solution
        ##  i.e. r = R*Rhat + rmag*rhat
        self.R    = np.zeros(nobs)
        self.Rhat = np.zeros((nobs,3))
        self.rhat = np.zeros((nobs,3))
        self.rmag = np.zeros(nobs)
        
    def minimize_solution(self, verbose = True):
        ''' 
            we solve for x0, y0, z0, vx, vy, vz, r
            so that X + V*t = Rk + r rkj for all k sites
            where X   = (x0, y0, z0)
                  V   = (vx, vy, vz)
                  Rk  = position of obs site k
                  rkj = line of sight of bolide from obs site k for jth timestep
        '''

        ### number of timestamps in both GLM16 and GLM17
        ntk = np.array([obsi.t.shape[0] for obsi in self.Obslist])
        nt  = np.sum(ntk)
        
        ### therefore, there are 3*nt equations and 6 + nt unknowns (x0, y0,z0, vx, vy, vz) and nt rkj 's 
        J  = np.zeros((3*nt, 6+nt))
        b  = np.zeros(3*nt)

        rhat   = np.zeros((nt,3))
        Rhat   = np.zeros((nt,3))
        self.t = np.zeros(nt)

        for k in range(self.nobs):
            ### bookkeeping... rhat is stored as a giant matrix with all sites
            ### so this gets the starting index based on the number of timestamps
            ### in all observing site before the current one
            offset = sum(ntk[:k])
            obsi   = self.Obslist[k]

            for i in range(ntk[k]):
                rhat[i+offset,:]   = obsi.rhat[i,:]
                Rhat[i+offset,:]   = obsi.Rhat
                self.t[i+offset]   = obsi.t[i]


        ''' 
            now, initialize the variables that we will solve for
        '''
        X = np.zeros(6+nt)

        done = False
        while(done==False):

            ### fill in the Jacobian and b-vector
            for i in range(nt):
                ti = self.t[i]

                ### x-coordinate
                J[3*i,0]     = -1. ## du/dx0
                J[3*i,3]     = -ti ## du/dvx
                J[3*i,6+i]   = rhat[i,0]  ### du/dr

                ### y-coordinate
                J[3*i+1,1]   = -1. ## dv/dy0
                J[3*i+1,4]   = -ti ## dv/dvy
                J[3*i+1,6+i] = rhat[i,1] ## dv/dr

                ### z-coordinate
                J[3*i+2,2]   = -1. ## dw/dz0
                J[3*i+2,5]   = -ti ## dw/dvz
                J[3*i+2,6+i] = rhat[i,2] ## dw/dr

                b[3*i]    = -(Rhat[i,0] + X[6+i]*rhat[i,0] - X[0] - X[3]*ti)
                b[3*i+1]  = -(Rhat[i,1] + X[6+i]*rhat[i,1] - X[1] - X[4]*ti)
                b[3*i+2]  = -(Rhat[i,2] + X[6+i]*rhat[i,2] - X[2] - X[5]*ti)

            JTJinv = np.linalg.inv(np.dot(J.T,J))
            dx = np.asarray(np.dot(np.dot(JTJinv,J.T),b))
            X  = X + dx
            # print(X[:6])
            # # print(X.shape)

            if(np.linalg.norm(dx) < 1.e-3):
                done = True

        ''' RETRIEVE THE FIT PARAMETERS '''
        x0 = X[0]
        y0 = X[1]
        z0 = X[2]

        vx = X[3]
        vy = X[4]
        vz = X[5]

        r  = X[6:]
        self.trajdir  = np.array([vx, vy, vz])
        self.trajdir  = self.trajdir/np.linalg.norm(self.trajdir)

        bolidexfit1d  = np.array([vx, x0])
        bolideyfit1d  = np.array([vy, y0])
        bolidezfit1d  = np.array([vz, z0])

        bolidexfit1df = np.poly1d(bolidexfit1d)
        bolideyfit1df = np.poly1d(bolideyfit1d)
        bolidezfit1df = np.poly1d(bolidezfit1d)

        self.bolidexfit1df = bolidexfit1df
        self.bolideyfit1df = bolideyfit1df
        self.bolidezfit1df = bolidezfit1df

        self.resarray = []

        dx = 0.; dy = 0.; dz = 0.
        dvx = 0.; dvy = 0.; dvz = 0.

        ''' unpack the result and distribute for each site '''
        for k in range(self.nobs):
            offset = np.sum(ntk[:k])

            mask = range(offset,offset+ntk[k])
            resi = {}
            resi["t"]         = self.t[mask]

            resi["bolidepos"] = np.zeros((ntk[k],3))
            resi["diff"]      = np.zeros((ntk[k],3))
            resi["ldiff"]     = np.zeros(ntk[k])
            resi["lat"]       = np.zeros(ntk[k])
            resi["lon"]       = np.zeros(ntk[k])
            resi["h"]         = np.zeros(ntk[k])


            for i, ti in enumerate(resi["t"]):
                fitki = (X[0:3] + ti*X[3:6])
                bposi = Rhat[i+offset,:] + r[i+offset]*rhat[i+offset,:]
                resi["bolidepos"][i] = bposi
                resi["diff"][i]      = resi["bolidepos"][i] - fitki

                resi["ldiff"][i]     = np.linalg.norm(X[3:6]*ti) - \
                    np.linalg.norm(bposi - X[0:3] )

                resi["lat"][i], resi["lon"][i], resi["h"][i] = project_h(bposi)


                ''' calculate the residuals for each observation from the mean trajectory '''
                dx  += resi["diff"][i,0]**2.
                dy  += resi["diff"][i,1]**2.
                dz  += resi["diff"][i,2]**2.

                dvx += ((bposi[0]-x0)/ti - vx)**2.
                dvy += ((bposi[1]-y0)/ti - vy)**2.
                dvz += ((bposi[2]-z0)/ti - vz)**2.
            resi["lat"] = np.degrees(resi["lat"])
            resi["lon"] = np.degrees(resi["lon"])
            self.resarray.append(resi)

        ''' FIND THE ERRORS '''
        dx  = np.sqrt(dx/nt)
        dy  = np.sqrt(dy/nt)
        dz  = np.sqrt(dz/nt)

        dvx = np.sqrt(dvx/nt)
        dvy = np.sqrt(dvy/nt)
        dvz = np.sqrt(dvz/nt)

        self.dx = dx; self.dy = dy; self.dz = dz
        self.dvx = dvx; self.dvy = dvy; self.dvz = dvz

        self.dX = np.max([dx, dy, dz])
        self.dV = np.max([dvx, dvy, dvz])

        ''' GET THE TRAJECTORY INFO AND RADIANT '''
        vinf = np.linalg.norm([vx,vy,vz])

        ''' zenith distance'''
        rhat = np.asarray([bolidexfit1d[1],bolideyfit1d[1],bolidezfit1d[1]])/\
            np.linalg.norm([bolidexfit1d[1],bolideyfit1d[1],bolidezfit1d[1]])

        radiant = np.arccos(np.clip(np.dot(-rhat, self.trajdir), 0., 1.))
        ### by calculating sigma(rhat dot trajdir)
        sigma_vm = (1./(2.*vinf))*np.linalg.norm([2.*dvx*vx, 2*dvy*vy, 2*dvz*vz])

        r0       = [x0, y0, z0]
        rm       = np.linalg.norm(r0)
        sigma_rm = (1./(2.*rm))*np.linalg.norm([2.*r0[0]*dx, 2*dy*r0[1], 2*dz*r0[2]])

        sigma_a  = (vx/vinf)*(r0[0]/rm)*np.sqrt(((sigma_vm/vinf)**2.)+((dvx/vx)**2.) + ((dx/r0[0])**2.) + ((sigma_rm/rm)**2.))
        sigma_b  = (vy/vinf)*(r0[1]/rm)*np.sqrt(((sigma_vm/vinf)**2.)+((dvy/vy)**2.) + ((dy/r0[1])**2.) + ((sigma_rm/rm)**2.))
        sigma_c  = (vz/vinf)*(r0[2]/rm)*np.sqrt(((sigma_vm/vinf)**2.)+((dvz/vz)**2.) + ((dz/r0[2])**2.) + ((sigma_rm/rm)**2.))

        ddot = np.linalg.norm([sigma_a, sigma_b, sigma_c])


        dradiant = 1./(1. - 0.73955**2.)*ddot

        ''' azimuth '''
        lat = np.zeros_like(self.t)
        lon = np.zeros_like(self.t)
        for i, ti in enumerate(self.t):
            bfiti = np.asarray([bolidexfit1df(ti), bolideyfit1df(ti), bolidezfit1df(ti)])
            lat[i], lon[i], _ = project_h(bfiti)
        lat = np.degrees(lat)
        lon = np.degrees(lon)

        g = pyproj.Geod(ellps="WGS84")
        ar_fit , _, _ = g.inv(lon[-1], lat[-1], lon[0], lat[0])
        ar_fit2, _, _ = g.inv(lon[-1]+0.002, lat[-1], lon[0]-0.002, lat[0])
        ar_fit3, _, _ = g.inv(lon[-1]-0.002, lat[-1], lon[0]+0.002, lat[0])

        dar = np.abs(ar_fit3 - ar_fit2)

        ''' celestial radiant '''
        t = Time(self.ref,scale='utc',location=('0d','0d'))
        LST = t.sidereal_time('apparent').value*(np.pi/12.)
        r = np.array([x0, y0, z0])
        v = vinf*self.trajdir

        theta0, lambda0, _ = project_h(r)

        ### Coordinate transforms to convert Geocentric to RA/DEC ##
        # rotatepLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), LST)
        # rotatemLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), -LST)

        # vrot  = (2.*np.pi*np.linalg.norm(r))*np.cos(lambda0)/86164.09*np.array([np.cos(theta0), np.sin(theta0), 0.])
        # vrot  = np.cross(np.array([0., 0., 1.]), vrot)

        # v = v - rotatepLST(vrot)

        # loc   = coord.EarthLocation(x=r[0]*u.m, y=r[1]*u.m, z=r[2]*u.m)
        # # altaz = coord.SkyCoord(az=ar_fit*u.deg, alt=(90.-np.degrees(radiant))*u.deg, location=loc, obstime=t, frame='altaz')
        # altaz = coord.SkyCoord(x=-v[0]*u.m, y=-v[1]*u.m, z=-v[2]*u.m, frame='altaz', obstime=t, representation_type='cartesian', location=loc)

        # print(altaz)

        itrs = coord.ITRS(x=-v[0]*u.m, y=-v[1]*u.m, z=-v[2]*u.m, obstime=t, representation_type='cartesian')
    
        radec = itrs.transform_to(coord.GCRS(obstime=t))

        RA    = radec.ra.value
        DEC   = radec.dec.value

        self.vpoly = np.poly1d([vinf])
        accall = - (9.8)*rhat

        lxfit = lambda t: vx*t
        lyfit = lambda t: vy*t
        lzfit = lambda t: vz*t

        bolidexfit = lambda t: bolidexfit1d[1] + lxfit(t) + (0.5*accall[0]*t**2.)
        bolideyfit = lambda t: bolideyfit1d[1] + lyfit(t) + (0.5*accall[1]*t**2.)
        bolidezfit = lambda t: bolidezfit1d[1] + lzfit(t) + (0.5*accall[2]*t**2.)

        # self.vpoly = vpoly
        self.bolidefit = lambda t: np.asarray([bolidexfit(t), bolideyfit(t), bolidezfit(t)])
        self.radiant   = radiant

        self.theta = np.zeros_like(self.t)
        self.phi   = np.zeros_like(self.t)
        self.h     = np.zeros_like(self.t)
        
        self.traj  = np.zeros((self.t.shape[0],3))
        self.dtall = np.sort(self.t)
        self.vinf  = vinf

        for i, ti in enumerate(self.dtall):
            self.traj[i,:]   = self.bolidefit(ti)
            thetai, phii, hi = project_h(self.traj[i,:])
            self.theta[i]    = np.degrees(thetai)
            self.phi[i]      = np.degrees(phii)
            self.h[i]        = hi

        solshape = (self.dtall.shape[0], 1)
        V        = vinf*np.ones_like(self.dtall)

        ''' save the data '''
        ### Write out data
        timedata = np.hstack((\
            self.dtall.reshape(solshape), self.traj, self.h.reshape(solshape), V.reshape(solshape), self.phi.reshape(solshape), self.theta.reshape(solshape))\
        )
        np.savetxt('timedata.csv', timedata, delimiter=',', fmt="%.5f")

        if(verbose):
            print("Vinf: %.3f pm %.3f"%(vinf, self.dV)) 
            print("Zenith dist of radiant: %.3f pm %.3f"%(np.degrees(radiant), np.degrees(dradiant)))
            print("Azimuth: %.3f pm %.3f"%(ar_fit, dar))
            print("Radiant RA: %.2f deg\t DEC: %.2f deg"%(RA, DEC))
            print("Starting time: %.3f height: %.3f "%(self.dtall[0], self.h[0]))
            print("\t \t Lat: %.3f Lon: %.3f"%(self.theta[0], self.phi[0]))
            print("End time: %.3f; End height: %.3f"%(self.dtall[-1], self.h[-1]))
            print("\t \t Lat: %.3f Lon: %.3f"%(self.theta[-1], self.phi[-1]))

    
    def do_plots(self):
        ### Check for plots folder
        if not os.path.exists('plots/'):
            os.makedirs('plots/')
            print("Folder was created: ", 'plots/')

        ### PLOT HEIGHT ###
        fig1 = plt.figure();
        ax4 = fig1.add_subplot(111)
        for k in range(self.nobs):
            ax4.plot(self.resarray[k]["t"], self.resarray[k]["h"]/1000., '.', label=self.Obslist[k].name)
        ax4.plot(self.dtall, self.h/1000., 'b-')
        ax4.set_xlabel('Time [s]')
        ax4.set_ylabel('Height [km]')
        ax4.set_ylim((0., np.max(self.h/1000.)*1.05))
        ax4.set_title("Height vs. time")
        plt.legend(loc='upper right')

        ### PLOT latlong ###
        fig4 = plt.figure(); ax7 = fig4.add_subplot(111)
        ax7.set_title("Map projected trajectory")
        ax7.plot(self.phi, self.theta, 'k-')
        ax7.plot(self.phi[0], self.theta[0], 'ko')

        # ax7.plot(resarray[0]["lon"], resarray[0]["lat"], '.', label="GLM16")
        # ax7.plot(resarray[1]["lon"], resarray[1]["lat"], '.', label="GLM17")
        for k in range(self.nobs):
            ax7.plot(self.resarray[k]["lon"], self.resarray[k]["lat"], '.', label=self.Obslist[k].name)
        # ax7.plot(node34.lon, node34.lat, 'k.')
        # ax7.plot(node42.lon, node42.lat, 'k.')
        # ax7.plot(node10.lon, node10.lat, 'k.')
        ax7.set_xlabel('Longitude [deg]')
        ax7.set_ylabel('Latitude [deg]')
        ax7.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False))

        fig1.tight_layout()
        fig1.savefig('plots/VHt.png')

        fig4.tight_layout()
        fig4.savefig('plots/latlong.png')

        ### plot x,y,z diff 
        figdiff = plt.figure()
        axxdiff = figdiff.add_subplot(311)
        axydiff = figdiff.add_subplot(312)
        axzdiff = figdiff.add_subplot(313)

        for k in range(self.nobs):
            axxdiff.plot(self.resarray[k]["t"], self.resarray[k]["diff"][:,0]/1000., '.', label=self.Obslist[k].name)
            axydiff.plot(self.resarray[k]["t"], self.resarray[k]["diff"][:,1]/1000., '.', label=self.Obslist[k].name)
            axzdiff.plot(self.resarray[k]["t"], self.resarray[k]["diff"][:,2]/1000., '.', label=self.Obslist[k].name)

        axxdiff.axhline(0.)
        axydiff.axhline(0.)
        axzdiff.axhline(0.)

        axxdiff.set_ylabel("dx [km]")
        axydiff.set_ylabel("dy [km]")
        axzdiff.set_ylabel("dz [km]")
        axzdiff.set_xlabel("Time [s]")

        axxdiff.set_title("x,y,z residuals")

        figdiff.tight_layout()

        figdiff.savefig('plots/xyzdiff.png')

        ### plot ldiff
        figldiff =  plt.figure()
        axldiff  = figldiff.add_subplot(111)
        for k in range(self.nobs):
            axldiff.plot(self.resarray[k]["t"], self.resarray[k]["ldiff"]/1000., '.', label=self.Obslist[k].name)
        axldiff.axhline(0.)

        axldiff.set_title('Length residuals')
        axldiff.set_xlabel("Time [s]")
        axldiff.set_ylabel("Deviation from expected length [km]")
        axldiff.set_ylim(-3, 3)
        plt.legend(loc='upper right')
        figldiff.tight_layout()
        figldiff.savefig('plots/ldiff.png')


        ''' plot the map as well '''
        self.plot_map()

    def get_orbit_old(self, tfinal=2.0, planets=["Venus", "Earth", "Mars", "Jupiter", "Saturn"]):
        t = Time(self.ref,scale='utc',location=('0d','0d'))
        LST = t.sidereal_time('apparent').value*(np.pi/12.)
        r = self.traj[0,:]
        v = self.vinf*self.trajdir

        theta0, lambda0, _ = project_h(r)

        ''' convert the local GCRS coordinate to Heliocentric Elliptic '''
        itrsloc = coord.ITRS(x=r[0]*u.m, y=r[1]*u.m, z=r[2]*u.m,\
             v_x=v[0]*u.m/u.s, v_y=v[1]*u.m/u.s, v_z=v[2]*u.m/u.s,\
             representation_type='cartesian', differential_type='cartesian', obstime=t)

        gcrsloc = itrsloc.transform_to(coord.GCRS(obstime=t))
        # print(gcrsloc)

        heliocentricloc = gcrsloc.transform_to(coord.HeliocentricTrueEcliptic(obstime=t,equinox=t))

        helior = heliocentricloc.cartesian
        heliov = heliocentricloc.velocity
        rHelio = np.array([helior.x.value, helior.y.value, helior.z.value])
        vHelio = np.array([heliov.d_x.value, heliov.d_y.value, heliov.d_z.value])*1000.

        ''' OLD CONVERSION CODE -- DEPRECATED '''        
        # print(rHelio, vHelio)

        # # ### Coordinate transforms to convert Geocentric to RA/DEC ##
        # rotatepLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), LST)
        # rotatemLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), -LST)

        # vrot  = (2.*np.pi*np.linalg.norm(r))*np.cos(lambda0)/86164.09*np.array([np.cos(theta0), np.sin(theta0), 0.])
        # vrot  = np.cross(np.array([0., 0., 1.]), vrot)

        # v = v + vrot

        # r  = rotatepLST(r)
        # v  = rotatepLST(v)

        # # Tilt of the Earth
        # rSun = rotationAbout(r, np.asarray([1., 0., 0.]), -23.4*np.pi/180.)
        # vSun = rotationAbout(v, np.asarray([1., 0., 0.]), -23.4*np.pi/180.)

        # rHelio2 = rSun
        # vHelio2 = vSun

        # print(rHelio2, vHelio2)

        sunUnits = Units()
        sunUnits.set_units(1.988e30, 1.496e11, 3.15e7)

        ## start rebound sim
        sim = rebound.Simulation()

        # integrator options
        sim.units = ('yr','AU','Msun')
        sim.integrator = "ias15"
        sim.dt = -1.e-5
        sim.ri_ias15.min_dt = -1e-6 #
        sim.testparticle_type = 1

        # collision and boundary options
        sim.collision = "direct"
        sim.collision_resolve = "merge"

        sim.add(m=1.)

        ## get JD
        jd = t.jd

        ## set up the planet codes for importing from Horizons
        planetcodes = {"Mercury":199, "Venus":299, "Earth": 399, "Mars": 499, "Jupiter": 599, "Saturn": 699, "Uranus":799, "Neptune": 899}
        mPlanets = {"Mercury":0.3e24, "Venus": 4.87e24, "Earth": 5.97e24, "Mars":0.642e24, "Jupiter": 1.898e27, "Saturn":5.67e26, "Uranus": 8.68e25, "Neptune": 1.02e26}

        ## intialize empty dicts to hold planet data
        orbs = {}
        rPlan = {}
        vPlan = {}
        objdat = {}
        objdat["Sun"] = {"pos": np.asarray([0., 0., 0.]), "vel": np.asarray([0., 0., 0.]), "mass": 1.99e30}

        ## get planetary positions
        for planet in planets:
            jdate = "JD%f"%(jd)

            sim.add("%s"%(planetcodes[planet]), date=jdate)

            orbs[planet] = Orbit()
            
            rPlan[planet] = np.asarray([sim.particles[-1].x, sim.particles[-1].y, sim.particles[-1].z])
            vPlan[planet] = np.asarray([sim.particles[-1].vx, sim.particles[-1].vy, sim.particles[-1].vz])

            orbs[planet].create_from_rv(rPlan[planet], vPlan[planet], sunUnits)

            objdat[planet] = {"pos": rPlan[planet], "vel": vPlan[planet], "mass": mPlanets[planet]}


        ''' convert units '''
        rHelio = (rHelio)/1.496e11
        vHelio = (vHelio)*(3.15e7/1.496e11)

        sim.N_active=sim.N

        ''' add the asteroid to the sim '''
        sim.add(m=1.e-20, x=rHelio[0], y=rHelio[1], z=rHelio[2], vx=vHelio[0], vy=vHelio[1], vz=vHelio[2])
        sim.move_to_com()

        ''' run the sim to the final timestamp '''
        sim.integrate(-tfinal,exact_finish_time=0)

        ''' get the orbital elements '''
        orbits = sim.calculate_orbits()
        particles = sim.particles
        asteroid = particles[-1]
        astorbit = orbits[-1]

        ''' get the final position of the asteroid after integration '''
        rSun = np.asarray([particles[-1].x, particles[-1].y, particles[-1].z])
        vSun = np.asarray([particles[-1].vx, particles[-1].vy, particles[-1].vz])

        a = astorbit.a
        e = astorbit.e
        i = np.degrees(astorbit.inc)
        Omega = np.degrees(astorbit.Omega)
        omega = np.degrees(astorbit.omega)
        tanomaly = np.degrees(astorbit.f)

        peri = a*(1.-e)
        apo = a*(1. + e)


        ''' calculate tisserand parameter for Jupiter '''
        aJ = 5.2
        tJ = aJ/a + 2.*np.cos(np.radians(i))*np.sqrt((a/aJ)*(1. - e**2.))

        print()
        print("a: %.3f AU \ne: %.4f \ni: %.3f deg \nOmega: %.3f deg \nomega: %.3f deg"%( a, e, i, Omega, omega))
        print()
        print("q: %.3f AU \napo: %.3f AU"%(peri, apo))
        print("T_j = %.3f"%(tJ))

        ''' create the orbit for plotting '''
        sunorb = Orbit()
        sunorb.set_orbit_elem(a*(1.-e**2.), e, i, Omega, Omega + omega, Omega + omega + tanomaly, sunUnits, d=1)

        ## DO PLOTS ##
        nulist = np.linspace(sunorb.node_dir, sunorb.node_dir + 2.*np.pi, 800)

        asteroid = np.zeros((nulist.shape[0],3))
        planPos = {}
        for planet in planets:
            planPos[planet] = np.zeros_like(asteroid)
        for i, nui in enumerate(nulist):
            for planet in planets:
                planPos[planet][i] = orbs[planet].get_pos_nu(nui)
            asteroid[i,:] = sunorb.get_pos_nu(nui)

        
        ''' get the different sectors of the orbit for plot formatting '''
        peri = sunorb.get_pos_nu(sunorb.nu_0)
        apo  = sunorb.get_pos_nu(sunorb.nu_0 + np.pi)
        up   = np.where(asteroid[:,2] > 0.)[0]
        down = np.where(asteroid[:,2] < 0.)[0]


        ''' plot parameters '''
        linewidth=2
        markersize = 6
        sunmarker = 7
        plotsize = 1.05*np.max([ \
            np.max(np.linalg.norm(asteroid,axis=1)), np.max([ \
                np.max(np.linalg.norm(planPos[planet],axis=1)) for planet in planets] ) \
            ])

        fig1 = plt.figure(figsize=(10,5))

        colors = {"Mercury": 'gray', "Venus": 'g', "Earth": 'b', "Mars": 'r', "Jupiter": "sienna", "Saturn": 'orangered', "Uranus": 'dodgerblue', "Neptune": 'darkblue'}
        ax1 = fig1.add_subplot(121)

        ax1.plot(0., 0., 'yo',markersize=sunmarker)

        ''' plot the planet positions '''
        for planet in planets:
            ax1.plot(rPlan[planet][0], rPlan[planet][1], 'o', color=colors[planet],markersize=markersize)
            ax1.plot(planPos[planet][:,0], planPos[planet][:,1], '-', color=colors[planet],linewidth=linewidth)

        ax1.plot(asteroid[up,0], asteroid[up, 1], 'k-',linewidth=linewidth)
        ax1.plot(asteroid[down,0], asteroid[down, 1], 'k--',linewidth=linewidth)
        ax1.plot(peri[0], peri[1], 'k.',markersize=markersize)
        ax1.plot(apo[0], apo[1], 'ko',markersize=markersize)


        ax1.arrow(-(3.15)*plotsize/4., 3.*plotsize/4., 0.3, 0)
        ax1.arrow(-(3.15)*plotsize/4., 3.*plotsize/4., 0., 0.3)
        ax1.text(-(3.15)*plotsize/4. + 0.35, 3.*plotsize/4. - 0.02, r"x", )
        ax1.text(-(3.15)*plotsize/4. - 0.02, 3.*plotsize/4. + 0.35, r"y", )

        ax1.text(0.9, 0.9, r"(a)", transform = ax1.transAxes, )

        ax1.set_aspect('equal')
        ax1.set_xlim((-plotsize, plotsize))
        ax1.set_ylim((-plotsize, plotsize))


        nulist = np.linspace(-np.pi/2. + 0.1, 3.*np.pi/2., 800, endpoint=False)

        asteroid = np.zeros((nulist.shape[0],3))
        planPos = {}
        for planet in planets:
            planPos[planet] = np.zeros_like(asteroid)
        for i, nui in enumerate(nulist):
            for planet in planets:
                planPos[planet][i] = orbs[planet].get_pos_nu(nui)
            asteroid[i,:] = sunorb.get_pos_nu(nui)

        ax2 = fig1.add_subplot(122)

        up = np.where(asteroid[:,0] >= 0.)[0]
        down = np.where(asteroid[:,0] < 0.)[0]

        ax2.plot(asteroid[down,1], asteroid[down, 2], 'k--',linewidth=linewidth)
        for planet in planets:
            ax2.plot(planPos[planet][400:,1], planPos[planet][400:,2], '-', color=colors[planet],linewidth=linewidth)
        ax2.plot(0., 0., 'yo',markersize=sunmarker)
        for planet in planets:
            ax2.plot(rPlan[planet][1], rPlan[planet][2], 'o', color=colors[planet],markersize=markersize)
            ax2.plot(planPos[planet][:400,1], planPos[planet][:400,2], '-', color=colors[planet],linewidth=linewidth)
        ax2.plot(asteroid[up,1], asteroid[up, 2], 'k-',linewidth=linewidth)

        ax2.arrow(-3.*plotsize/4., 3.*plotsize/4., 0.3, 0)
        ax2.arrow(-3.*plotsize/4., 3.*plotsize/4., 0., 0.3)
        ax2.text(-3.*plotsize/4. + 0.35, 3.*plotsize/4. - 0.02, r"y", )
        ax2.text(-3.*plotsize/4. - 0.02, 3.*plotsize/4. + 0.35, r"z", )
        ax2.text(0.9, 0.9, r"(b)", transform = ax2.transAxes, )

        peri = sunorb.get_pos_nu(sunorb.nu_0)
        apo = sunorb.get_pos_nu(sunorb.nu_0 + np.pi)

        ax2.plot(peri[1], peri[2], 'k.',markersize=markersize)
        ax2.plot(apo[1], apo[2], 'ko',markersize=markersize)

        plotsize = 2.

        ax2.set_aspect('equal')
        ax2.set_xlim((-plotsize, plotsize))
        ax2.set_ylim((-plotsize, plotsize))

        ax1.set_xticklabels([])
        ax1.set_yticklabels([])
        ax2.set_xticklabels([])
        ax2.set_yticklabels([])

        plt.tight_layout()
        fig1.savefig('plots/orbit.png',dpi=600)

    def get_orbit(self, tfinal=2.0, planets=["Venus", "Earth", "Mars", "Jupiter", "Saturn"], nsims=5000):
        t = Time(self.ref,scale='utc',location=('0d','0d'))
        LST = t.sidereal_time('apparent').value*(np.pi/12.)

        ''' OLD CONVERSION CODE -- DEPRECATED '''        
        # print(rHelio, vHelio)

        # # ### Coordinate transforms to convert Geocentric to RA/DEC ##
        # rotatepLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), LST)
        # rotatemLST = lambda u : rotationAbout(u, np.asarray([0., 0., 1.]), -LST)

        # vrot  = (2.*np.pi*np.linalg.norm(r))*np.cos(lambda0)/86164.09*np.array([np.cos(theta0), np.sin(theta0), 0.])
        # vrot  = np.cross(np.array([0., 0., 1.]), vrot)

        # v = v + vrot

        # r  = rotatepLST(r)
        # v  = rotatepLST(v)

        # # Tilt of the Earth
        # rSun = rotationAbout(r, np.asarray([1., 0., 0.]), -23.4*np.pi/180.)
        # vSun = rotationAbout(v, np.asarray([1., 0., 0.]), -23.4*np.pi/180.)

        # rHelio2 = rSun
        # vHelio2 = vSun

        # print(rHelio2, vHelio2)

        sunUnits = Units()
        sunUnits.set_units(1.988e30, 1.496e11, 3.15e7)

        ## start rebound sim
        sim = rebound.Simulation()

        # integrator options
        sim.units = ('yr','AU','Msun')
        sim.integrator = "ias15"
        sim.dt = -1.e-4
        sim.ri_ias15.min_dt = -1e-7 #
        sim.testparticle_type = 1

        # turn off collisions
        sim.collision = "none"

        sim.add(m=1.)

        ## get JD
        jd = t.jd

        ## set up the planet codes for importing from Horizons
        planetcodes = {"Mercury":199, "Venus":299, "Earth": 399, "Mars": 499, "Jupiter": 599, "Saturn": 699, "Uranus":799, "Neptune": 899}
        mPlanets = {"Mercury":0.3e24, "Venus": 4.87e24, "Earth": 5.97e24, "Mars":0.642e24, "Jupiter": 1.898e27, "Saturn":5.67e26, "Uranus": 8.68e25, "Neptune": 1.02e26}

        ## intialize empty dicts to hold planet data
        orbs = {}
        rPlan = {}
        vPlan = {}
        objdat = {}
        objdat["Sun"] = {"pos": np.asarray([0., 0., 0.]), "vel": np.asarray([0., 0., 0.]), "mass": 1.99e30}

        print()
        ## get planetary positions
        for planet in planets:
            jdate = "JD%f"%(jd)

            sim.add("%s"%(planetcodes[planet]), date=jdate)

            orbs[planet] = Orbit()
            
            rPlan[planet] = np.asarray([sim.particles[-1].x, sim.particles[-1].y, sim.particles[-1].z])
            vPlan[planet] = np.asarray([sim.particles[-1].vx, sim.particles[-1].vy, sim.particles[-1].vz])

            orbs[planet].create_from_rv(rPlan[planet], vPlan[planet], sunUnits)

            objdat[planet] = {"pos": rPlan[planet], "vel": vPlan[planet], "mass": mPlanets[planet]}

        sim.N_active=sim.N    

        ''' add the particles '''
        r = self.traj[0,:]
        v = self.vinf*self.trajdir

        # ''' get the number of simulations to run '''
        # dx  = [-2*self.dx,  -self.dx,  0., self.dx,  2*self.dx]
        # dy  = [-2*self.dy,  -self.dy,  0., self.dy,  2*self.dy]
        # dz  = [-2*self.dz,  -self.dz,  0., self.dz,  2*self.dz]
        # dvx = [-2*self.dvx, -self.dvx, 0., self.dvx, 2*self.dvx]
        # dvy = [-2*self.dvy, -self.dvy, 0., self.dvy, 2*self.dvy]
        # dvz = [-2*self.dvz, -self.dvz, 0., self.dvz, 2*self.dvz]

        # a = [dx, dy, dz, dvx, dvy, dvz]
        # clist = list(itertools.product(*a))

        ''' create random perturbations of the original trajectory '''
        da = (np.random.rand(nsims,6) - 0.5)*2.

        da[:,0] = da[:,0]*self.dx
        da[:,1] = da[:,1]*self.dy
        da[:,2] = da[:,2]*self.dz
        da[:,3] = da[:,3]*self.dvx
        da[:,4] = da[:,4]*self.dvx
        da[:,5] = da[:,5]*self.dvx

        aarr    = np.zeros(nsims)
        incarr  = np.zeros(nsims)
        periarr = np.zeros(nsims)
        eccarr  = np.zeros(nsims)
        nodearr = np.zeros(nsims)
        argparr = np.zeros(nsims)
        print("Adding %d particles..."%(nsims))

        rarr = np.zeros((nsims,3))
        varr = np.zeros((nsims,3))

        rarr[:,0] = r[0] + da[:,0]
        rarr[:,1] = r[1] + da[:,1]
        rarr[:,2] = r[2] + da[:,2]
        varr[:,0] = v[0] + da[:,3]
        varr[:,1] = v[1] + da[:,4]
        varr[:,2] = v[2] + da[:,5]

        ''' convert the local GCRS coordinate to Heliocentric Elliptic '''
        itrsloc = coord.ITRS(x=rarr[:,0]*u.m, y=rarr[:,1]*u.m, z=rarr[:,2]*u.m,\
            v_x=varr[:,0]*u.m/u.s, v_y=varr[:,1]*u.m/u.s, v_z=varr[:,2]*u.m/u.s,\
            representation_type='cartesian', differential_type='cartesian', obstime=t)

        heliocentricloc = itrsloc.transform_to(coord.HeliocentricTrueEcliptic(obstime=t,equinox=t))

        helior = heliocentricloc.cartesian
        heliov = heliocentricloc.velocity

        for i in range(nsims):
            # progress = (i/nsims)*100
            # nlines   = int(progress/5)
            # sys.stdout.write("\r\r [%-20s] %.1f%%"%('='*nlines,progress))
            # sys.stdout.flush()

            rHelio = np.array([helior[i].x.value, helior[i].y.value, helior[i].z.value])
            vHelio = np.array([heliov[i].d_x.value, heliov[i].d_y.value, heliov[i].d_z.value])*1000.
            
            rHelio = (rHelio)/1.496e11
            vHelio = (vHelio)*(3.15e7/1.496e11)

            sim.add(m=1.e-20, x=rHelio[0], y=rHelio[1], z=rHelio[2], vx=vHelio[0], vy=vHelio[1], vz=vHelio[2])
        print("Running sims...")
        # print("Check: %d + %d = %d"%(sim.N_active, nsims, sim.N))
        ''' start the simulation '''
        # move to center of mass
        sim.move_to_com()
        ## integrate backwards
        tlist = np.linspace(0., tfinal, 20)

        for i, ti in enumerate(tlist):
            progress = (i/20)*100
            nlines   = int(progress/5)
            sys.stdout.write("\r\r [%-20s] %.1f%%"%('='*nlines,progress))
            sys.stdout.flush()

            sim.integrate(-ti,exact_finish_time=0)
        print()

        ''' calculate the orbits '''
        particles = sim.particles
        orbits = sim.calculate_orbits()

        for i in range(nsims):
            astorbit = orbits[sim.N_active-1+i]

            a        = astorbit.a
            e        = astorbit.e
            inc      = np.degrees(astorbit.inc)
            Omega    = np.degrees(astorbit.Omega)
            omega    = np.degrees(astorbit.omega)
            tanomaly = np.degrees(astorbit.f)

            peri = a*(1.-e)
            apo  = a*(1.+e)

            aarr[i]    = a
            eccarr[i]  = e
            incarr[i]  = inc
            periarr[i] = peri
            argparr[i] = omega
            nodearr[i] = Omega

        a = np.average(aarr); astd = np.std(aarr)
        e = np.average(eccarr); estd = np.std(eccarr)
        i = np.average(incarr); istd = np.std(incarr)
        p = np.average(periarr); pstd = np.std(periarr)

        omega = np.average(argparr); apstd = np.std(argparr)
        Omega = np.average(nodearr); nistd = np.std(nodearr)

        if(omega < 0.):
            omega += 360.

        print()
        print("a: %.3f pm %.3f AU \ne: %.4f pm %.3f \ni: %.3f pm %.3f deg \nOmega: %.3f pm %.3f deg \nomega: %.3f pm %.3f deg"%\
            ( a, astd, e, estd, i, istd, Omega, nistd, omega, apstd))
        print()
        print("q: %.3f pm %.3f AU"%(p, pstd))

        ''' create the orbit for plotting '''
        sunorb = Orbit()
        sunorb.set_orbit_elem(a*(1.-e**2.), e, i, Omega, Omega + omega, Omega + omega + tanomaly, sunUnits, d=1)

        ## DO PLOTS ##
        nulist = np.linspace(sunorb.node_dir, sunorb.node_dir + 2.*np.pi, 800)

        asteroid = np.zeros((nulist.shape[0],3))
        planPos = {}
        for planet in planets:
            planPos[planet] = np.zeros_like(asteroid)
        for i, nui in enumerate(nulist):
            for planet in planets:
                planPos[planet][i] = orbs[planet].get_pos_nu(nui)
            asteroid[i,:] = sunorb.get_pos_nu(nui)

        
        ''' get the different sectors of the orbit for plot formatting '''
        peri = sunorb.get_pos_nu(sunorb.nu_0)
        apo  = sunorb.get_pos_nu(sunorb.nu_0 + np.pi)
        up   = np.where(asteroid[:,2] > 0.)[0]
        down = np.where(asteroid[:,2] < 0.)[0]


        ''' plot parameters '''
        linewidth=2
        markersize = 6
        sunmarker = 7
        plotsize = 1.1*np.max([ \
            np.max(np.linalg.norm(asteroid,axis=1)), np.max([ \
                np.max(np.linalg.norm(planPos[planet],axis=1)) for planet in planets] ) \
            ])

        fig1 = plt.figure(figsize=(10,5))

        colors = {"Mercury": 'gray', "Venus": 'g', "Earth": 'b', "Mars": 'r', "Jupiter": "sienna", "Saturn": 'orangered', "Uranus": 'dodgerblue', "Neptune": 'darkblue'}
        ax1 = fig1.add_subplot(121)

        ax1.plot(0., 0., 'yo',markersize=sunmarker)

        ''' plot the planet positions '''
        for planet in planets:
            ax1.plot(rPlan[planet][0], rPlan[planet][1], 'o', color=colors[planet],markersize=markersize)
            ax1.plot(planPos[planet][:,0], planPos[planet][:,1], '-', color=colors[planet],linewidth=linewidth)

        ax1.plot(asteroid[up,0], asteroid[up, 1], 'k-',linewidth=linewidth)
        ax1.plot(asteroid[down,0], asteroid[down, 1], 'k--',linewidth=linewidth)
        ax1.plot(peri[0], peri[1], 'k.',markersize=markersize)
        ax1.plot(apo[0], apo[1], 'ko',markersize=markersize)


        ax1.arrow(-(3.15)*plotsize/4., 2.9*plotsize/4., 0.15*plotsize, 0)
        ax1.arrow(-(3.15)*plotsize/4., 2.9*plotsize/4., 0., 0.15*plotsize)
        ax1.text(-(3.15)*plotsize/4. + 0.2*plotsize, 2.9*plotsize/4. - 0.02, r"x")
        ax1.text(-(3.15)*plotsize/4. - 0.02, 2.9*plotsize/4. + 0.2*plotsize, r"y")

        ax1.text(0.9, 0.9, r"(a)", transform = ax1.transAxes, )

        ax1.set_aspect('equal')
        ax1.set_xlim((-plotsize, plotsize))
        ax1.set_ylim((-plotsize, plotsize))


        nulist = np.linspace(-np.pi/2. + 0.1, 3.*np.pi/2., 800, endpoint=False)

        asteroid = np.zeros((nulist.shape[0],3))
        planPos = {}
        for planet in planets:
            planPos[planet] = np.zeros_like(asteroid)
        for i, nui in enumerate(nulist):
            for planet in planets:
                planPos[planet][i] = orbs[planet].get_pos_nu(nui)
            asteroid[i,:] = sunorb.get_pos_nu(nui)

        ax2 = fig1.add_subplot(122)

        up = np.where(asteroid[:,0] >= 0.)[0]
        down = np.where(asteroid[:,0] < 0.)[0]

        ax2.plot(asteroid[down,1], asteroid[down, 2], 'k--',linewidth=linewidth)
        for planet in planets:
            ax2.plot(planPos[planet][400:,1], planPos[planet][400:,2], '-', color=colors[planet],linewidth=linewidth)
        ax2.plot(0., 0., 'yo',markersize=sunmarker)
        for planet in planets:
            ax2.plot(rPlan[planet][1], rPlan[planet][2], 'o', color=colors[planet],markersize=markersize)
            ax2.plot(planPos[planet][:400,1], planPos[planet][:400,2], '-', color=colors[planet],linewidth=linewidth)
        ax2.plot(asteroid[up,1], asteroid[up, 2], 'k-',linewidth=linewidth)

        ax2.arrow(-(3.15)*plotsize/4., 2.9*plotsize/4., 0.15*plotsize, 0)
        ax2.arrow(-(3.15)*plotsize/4., 2.9*plotsize/4., 0., 0.15*plotsize)
        ax2.text(-(3.15)*plotsize/4. + 0.2*plotsize, 2.9*plotsize/4. - 0.02, r"x")
        ax2.text(-(3.15)*plotsize/4. - 0.02, 2.9*plotsize/4. + 0.2*plotsize, r"y")
        ax2.text(0.9, 0.9, r"(b)", transform = ax2.transAxes, )

        peri = sunorb.get_pos_nu(sunorb.nu_0)
        apo = sunorb.get_pos_nu(sunorb.nu_0 + np.pi)

        ax2.plot(peri[1], peri[2], 'k.',markersize=markersize)
        ax2.plot(apo[1], apo[2], 'ko',markersize=markersize)

        ax2.set_aspect('equal')
        ax2.set_xlim((-plotsize, plotsize))
        ax2.set_ylim((-plotsize, plotsize))

        ax1.set_xticklabels([])
        ax1.set_yticklabels([])
        ax2.set_xticklabels([])
        ax2.set_yticklabels([])

        plt.tight_layout()
        fig1.savefig('plots/orbit.png',dpi=600)

    def plot_map(self):
        latavg = self.theta.mean()
        lonavg = self.phi.mean()

        projection = ccrs.Mercator(central_longitude=lonavg)

        fig = plt.figure(figsize=(7,8))
        ax  = fig.add_subplot(111, projection=projection)
        plt.subplots_adjust(top=0.98, bottom=0.02, left=0.05, right=0.98)
        ax.set_extent([self.phi.min()-3., self.phi.max()+3., self.theta.min()-3, self.theta.max()+3.], crs=ccrs.PlateCarree())
        ax.coastlines(resolution='10m', color='black', linewidth=0.5)

        ax.plot(self.phi, self.theta, 'k-', transform=ccrs.PlateCarree())
        ax.plot(self.phi[0], self.theta[0], 'k.', transform=ccrs.PlateCarree())

        fig.savefig('plots/map.png')

def get_GLMeph(ephdata):
    data = nc.Dataset(ephdata,'r')

    xsat = data['x'][:]
    ysat = data['y'][:]
    zsat = data['z'][:]
    tsat = data['attitudeTimes'][:]
    nsat = tsat.shape[0]

    ref = datetime.datetime(2000,1,1,12,0,0,0)
    k   = np.array([0., 0., 1.])

    satpos = np.zeros((nsat,3))

    for i, ti in enumerate(tsat):
        dt  = datetime.timedelta(seconds=ti)
        t   = Time(ref+dt, scale='utc', location=('0d','0d'))
        LST = t.sidereal_time('apparent').value*(np.pi/12.)

        cartrep = coord.CartesianRepresentation(np.asarray([xsat[i], ysat[i], zsat[i]]), unit='m')
        saticrs = coord.GCRS(cartrep, obstime=t)

        # print(saticrs)

        # satecef = rotationAbout(saticrs, k, -LST)
        satecef = saticrs.transform_to(coord.ITRS(obstime=t))
        # print(satecef)
        sati    = coord.EarthLocation(*satecef.cartesian.xyz)
        satpos[i,:] = np.asarray([sati.x.value, sati.y.value, sati.z.value])

    return np.average(satpos, axis=0)
