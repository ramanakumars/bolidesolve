import matplotlib
# matplotlib.use("Agg")
from bolideSolve import *

''' data for 31 March 2019 over Florida '''
ref    = datetime.datetime(2019,3,31,3,54,5,000000)

node10 = SkySentinelObs('node10', 'data/node10dat.csv', ref)
node34 = SkySentinelObs('node34', 'data/node34dat.csv', ref)
node42 = SkySentinelObs('node42', 'data/node42dat.csv', ref)


''' ADD THE LIST OF OBSERVATIONS HERE '''
Obs = [node10, node34, node42]

''' RUN THE SOLVER '''
solver = triangulate(Obs, ref)
solver.minimize_solution()


''' PLOT THE RESULTS '''
solver.do_plots()